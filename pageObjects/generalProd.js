module.exports = function (browser){
    this.openBrowser = function() {
        browser
            .url('https://www.chip.de/')
            .resizeWindow(1400, 3000)
        return browser
    };//end openBrowser

    this.tuk1 = function(){
        browser
            .verify.ok(true, "'https://www.chip.de/' opened sucessfully")
            .useXpath()
            .waitForElementVisible('/html/body', 10000)
            .pause(5000)
            // Take one screenshot 
            .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/CHIP_Page.png')
            .pause(3000)
    }

    this.consentMessage = function() {
        browser
        //this is to remove the message and allow to use the page
        .useCss()
        .waitForElementPresent('#sp_message_container_598129',10000)//sp_message_container_575150
        .execute("document.getElementById('sp_message_container_598129').removeAttribute('style')")//to remove the consent message option1 
        .pause(5000)
        .execute("document.getElementsByTagName('html')[0].classList.remove('sp-message-open')")//to make it scrollable
        .pause(5000)
        .verify.ok(true, "Consent Message")
        //end of removing the message
    }

    this.switching = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[0];
            this.switchWindow(newWindow);
        })
    }

    this.switchingWin2 = function(){
        browser
        //Switch back to the first window
        .windowHandles(function(result) {
            var newWindow;
            newWindow = result.value[1];
            this.switchWindow(newWindow);
        })
    }


    this.back = function() {
        browser
        .execute(function () {
            window.history.back()
        })//to go to the previous page
        .pause(4000)
    }

    this.previews = function(){
        browser
        .back()
        consentMessage()
        browser
        .useXpath()
    }

    this.testKaufberatung = function(){
        browser
        //Test & Kaufberatung
        .useXpath()
        //.moveToElement('/html/body/header/div/div/nav/ul/li[2]/a',1,1)
        .pause(4000)
        .waitForElementPresent("/html/body/header/div/div/nav/ul/li[2]/a", 2000)
        .verify.visible("/html/body/header/div/div/nav/ul/li[2]/a")
        .verify.containsText('/html/body/header/div/div/nav/ul/li[2]/a','TEST & KAUFBERATUNG')
        .verify.ok(true,"Hover over'Test & Kaufberatung'")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Hover_testundkaufberatung.png')
        .pause(2000)
        .click("/html/body/header/div/div/nav/ul/li[2]/a")
        .verify.ok(true,"Click on 'Test & Kaufberatung'")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/clickon_testundkaufberatung.png')
        .pause(2000)
    }


    this.breadcrump = function(){
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/nav", 5000)//breadcrump area
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/nav")
        .verify.ok(true,"Check for breadcrump to be present")
    }

    this.mainHeadline = function(){
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[1]/header/h1")
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[1]/header/h1")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[1]/header/h1','Test & Kaufberatung')
        .verify.ok(true,"Check headline 'Test & Kaufberatung'")
    }

    this.briefDescription = function(){
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[1]/header/p")
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[1]/header/p")
        .verify.ok(true,"Check brief description at the top of the page")
    }

    this.image = function(){
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[2]", 3000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[1]/div[2]")
        //*[@id="teaser-tuk"]
        .verify.ok(true,"Check top image in the right side")
    }

    this.search = function(){
        browser
        .useXpath()
        //from /Test page
        //title in search
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[2]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[2]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[2]/h3','Jetzt Technikthemen auf CHIP.de suchen')
        .verify.ok(true,"Check title 'Jetzt Technikthemen auf CHIP.de suchen'")
        //search bar
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[2]/form/input[1]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[2]/form/input[1]")
        .verify.ok(true,"Check the serach bar")
        //search button
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[2]/form/button", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[2]/form/button")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[2]/form/button','Jetzt suchen')
        .verify.ok(true,"Check 'Jetzt suchen' button")
        //enter a value 
        .setValue("/html/body/div[3]/div/main/div/div[1]/section[2]/form/input[1]", "smartphone")
        .click('/html/body/div[3]/div/main/div/div[1]/section[2]/form/button')//Jetzt suchen
        .verify.ok(true,"Search for 'smartphone'")
        consentMessage()
        browser
        .useXpath()
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/search_smartphone.png')
        .pause(2000)
        //Click on Alle Ergebnisse tab
        .waitForElementPresent("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[2]/div/div/div[1]/span", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[2]/div/div/div[1]/span")
        .verify.containsText('/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[2]/div/div/div[1]/span','Alle Ergebnisse')
        .click('/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[2]/div/div/div[1]/span')
        .verify.ok(true,"Click on 'Alle Ergebnisse'tab ")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/search_AlleErgebnisse.png')
        .pause(2000)
    }
//pending to see if we test all tabs***************

    this.sortieren = function(){
        browser
        //sortieren nach in search
        .useXpath()
        .waitForElementPresent("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[1]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[1]")
        .verify.containsText('/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[1]','Sortieren nach')
        .verify.ok(true,"'Sortieren nach' appears")
        .pause(4000)
        //open dropdown
        .waitForElementPresent("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[1]/div[2]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[1]/div[2]")
        .click("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[1]/div[2]")
        .verify.ok(true,"Click on 'Sortieren nach' dropdown")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/dropdown_sortieren.png')
        .pause(2000)
        //select Datum
        .waitForElementPresent("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[2]/div[2]/div", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[2]/div[2]/div")
        .click('/html/body/div[2]/div/main/section[3]/div/section[1]/div/div/div/div/div/div[3]/table/tbody/tr/td[2]/div/div[2]/div[2]/div[2]/div')
        .verify.ok(true,"Click on Datum")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Datum_sortieren.png')
        .pause(2000)

    }


    this.emptySearch = function(){
        browser
        //enter an empty value in search. In search page
        .useXpath()
        //Clear value
        .waitForElementPresent("/html/body/div[2]/div/main/section[2]/form/input[6]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[2]/form/input[6]")
        .clearValue('/html/body/div[2]/div/main/section[2]/form/input[6]')
        .pause(4000)
        //click on Jeztz suchen button
        .waitForElementPresent("/html/body/div[2]/div/main/section[2]/form/button", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[2]/form/button")
        .click('/html/body/div[2]/div/main/section[2]/form/button')//Jetzt suchen
        .verify.ok(true,"Enter an empty value in search bar")
        consentMessage()
        browser
        .useXpath()
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/search_emptyValue.png')
        .pause(2000)
    }


    this.keywordLabel = function(){
        browser
        //click on the first keyword label when we have an empty result in search. In search page
        .useXpath()
        .waitForElementPresent("/html/body/div[2]/div/main/section[3]/div/section[1]/section/div/a[1]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[3]/div/section[1]/section/div/a[1]")
        .click('/html/body/div[2]/div/main/section[3]/div/section[1]/section/div/a[1]')
        .verify.ok(true,"Click on the first keyword")
        consentMessage()
        browser
        .useXpath()
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/search_emptyValue_keyword.png')
        .pause(2000)
    }


    this.invalidValue = function(){
        browser
        .useXpath()
        //Clear value
        .waitForElementPresent("/html/body/div[2]/div/main/section[2]/form/input[6]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[2]/form/input[6]")
        .clearValue('/html/body/div[2]/div/main/section[2]/form/input[6]')
        .verify.ok(true,"Clearing search bar")
        //enter an invalid value in search. In search page
        .waitForElementPresent("/html/body/div[2]/div/main/section[2]/form/input[6]", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[2]/form/input[6]")
        .setValue("/html/body/div[2]/div/main/section[2]/form/input[6]", "iygu6586g")
        .verify.ok(true,"Enter an invalid value in the search bar")
        //click on Jeztz suchen button
        .waitForElementPresent("/html/body/div[2]/div/main/section[2]/form/button", 2000)
        .verify.visible("/html/body/div[2]/div/main/section[2]/form/button")
        .click('/html/body/div[2]/div/main/section[2]/form/button')//Jetzt suchen
        consentMessage()
        browser
        .useXpath()
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/search_invalidValue.png')
        .pause(2000)
    }



    this.alleBestenlisten = function(){
        browser
        //alleBestenlisten
        .url('https://www.chip.de/tests')
        consentMessage()
        browser
        .useXpath()
        .waitForElementVisible('/html/body',5000)
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[1]/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[1]/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[1]/h2/a','ALLE BESTENLISTEN')
        .click('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[1]/h2/a')
        .verify.ok(true,"Click on Alle Bestenlisten")
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent("/html/body/main/div[2]", 5000)
        .verify.visible("/html/body/main/div[2]")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/alleBestenlisten.png')
        .pause(2000)
        back()
        consentMessage()

    }

    this.alleSchnappchen = function(){
        browser
        //Alle Schnäppchen
        .url('https://www.chip.de/tests')
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[2]/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[2]/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[2]/h2/a','ALLE SCHNÄPPCHEN')
        .click('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[2]/h2/a')
        .verify.ok(true,"Click on Alle Schnäppchen")
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent('//*[@id="M1"]', 5000)
        .verify.visible('//*[@id="M1"]')
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AlleSchnäppchen.png')
        .pause(2000)
        back()
        consentMessage()
    }

    this.alleCHIP365 = function(){
        browser
        //Alle Tests von CHIP 365
        .url('https://www.chip.de/tests')
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[3]/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[3]/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[3]/h2/a','ALLE TESTS VON CHIP 365')
        .click('/html/body/div[3]/div/main/div/div[1]/section[1]/div[2]/div[3]/h2/a')
        .verify.ok(true,"Click on Alle Tests von CHIP 365")
        consentMessage()
        browser
        .refresh()
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent('//*[@id="G32"]', 5000)
        .verify.visible('//*[@id="G32"]')
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AlleTestsvonCHIP365.png')
        .pause(2000)
        back()
        consentMessage()
    }

    this.aktuelleTestTeaser = function(){
        browser
        //check teasers in AKTUELLE TESTS
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[2]/div/div/section/div[1]/h3',940,17)
        .execute(function() { window.scrollTo(0, 500); }, [])
        .pause(4000)
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[2]/div/div/section/div[1]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[2]/div/div/section/div[1]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[2]/div/div/section/div[1]/h3','Aktuelle Tests')
        .verify.ok(true,"check teasers in AKTUELLE TESTS")
        //check all container
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[2]/div/div/section", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[2]/div/div/section")
        .verify.ok(true,"check all container of AKTUELLE TESTS to be present")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AKTUELLE TESTS.png')
        .pause(2000)

        //click on the links under 
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[2]/div/div/section", 2000)
        .element('xpath','/html/body/div[3]/div/main/div/div[2]/div/div/section', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[2]/div/div/section/article[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 4; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleTestTeaser/' + linkImage +'.png')
                .pause(4000)
                previews()
            }
            browser
            .useXpath()
            //click on the top right thumbnail
            .click('/html/body/div[3]/div/main/div/div[2]/div/div/section/div[2]/div/article[1]/a/figure/figcaption')
            consentMessage()
            browser
            .useXpath()
            .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
            .verify.visible("/html/body/div[3]/div/main/div/div[1]")
            .pause(4000)
            .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleTestTeaser/5.png')
            .pause(4000)
            previews()
         
            //click on the second right thumbnail
            browser
            .useXpath()
            .click('/html/body/div[3]/div/main/div/div[2]/div/div/section/div[2]/div/article[2]/a/figure/figcaption')
            consentMessage()
            browser
            .useXpath()
            .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
            .verify.visible("/html/body/div[3]/div/main/div/div[1]")
            .pause(4000)
            .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleTestTeaser/6.png')
            .pause(4000)
            previews()
            })
        
    }

    

    this.catHandy = function(){
        browser
        .useXpath()
        //Check headline "Alle Kaufberatungen und Vergleichstests"
        .waitForElementPresent('//*[@id="kaufberatungen-vergleichstests"]', 3000)
        .verify.containsText('//*[@id="kaufberatungen-vergleichstests"]','Alle Kaufberatungen und Vergleichstests')
        //check Handy & Zubehör category
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1500); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a','Handy & Zubehör')
        .verify.ok(true,"check Handy & Zubehör category")
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/categories_row1.png')
        .pause(2000)
        .click("/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a")
        .verify.ok(true,"Click on Handy & Zubehör category")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Handy&ZubehörCAT.png')
        .pause(3000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/a/figure/figcaption')
        .verify.ok(true,"Check Handy & Zubehör thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()

        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/ul", 2000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 6; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/handyCat/' + linkImage +'.png')
                .pause(4000)
                previews()
                browser
                //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/ul',272,17)
                .execute(function() { window.scrollTo(0, 1500); }, [])
              }
            })
    }


    this.pcnPeripherieCat = function(){
        browser
        //check PC & Peripherie category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1500); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/h2/a','PC & Peripherie')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/h2/a')
        .verify.ok(true,"Check PC & Peripherie")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/PC&PeripherieCat.png')
        .pause(2000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/a/figure/figcaption')
        .verify.ok(true,"Check PC & Peripherie thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()

        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[2]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 6; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/pcnPeripherieCat/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }

    this.tVnSoundCat = function(){
        browser
        //check TV & Sound category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1500); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/h2/a','TV & Sound')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/h2/a')
        .verify.ok(true,"Check TV & Sound")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/TV&SoundCat.png')
        .pause(2000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/a/figure/figcaption')
        .verify.ok(true,"Check TV & Sound thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()

        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[3]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 6; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/tVnSoundCat/' + linkImage +'.png')
                .pause(4000)
                previews()
       
              }
            })
    }

    this.notebookCat = function(){
        browser
        //check Notebooks & Co. category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/h2/a','Notebooks & Co.')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/h2/a')
        .verify.ok(true,"Notebooks & Co.")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/notebookCat.png')
        .pause(2000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/a/figure/figcaption')
        .verify.ok(true,"Check Notebooks & Co. category")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[4]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 6; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/notebookCat/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }

    this.smartwatch = function(){
        browser
        //check Smartwatch & Fitness-Trackercategory
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/h2/a','Smartwatch & Fitness-Tracker')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/h2/a')
        .verify.ok(true,"Check Smartwatch & Fitness-Tracker")
        consentMessage()
        browser
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Smartwatch&Fitness-Tracker.png')
        .pause(2000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/a/figure/figcaption')
        .verify.ok(true,"Check Smartwatch & Fitness-Tracker thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()

        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[5]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 4; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Smartwatch/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }

    this.tabletPcEbook= function(){
        browser
        //check Tablet-PC & eBook-Reader category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/h2/a','Tablet-PC & eBook-Reader')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/h2/a')
        .verify.ok(true,"Tablet-PC & eBook-Reader")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Tablet-PC&eBook-ReaderCat.png')
        .pause(4000)
        browser
        previews()
        //click on thumbnail
        browser
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/a/figure/figcaption')
        .verify.ok(true,"Tablet-PC & eBook-Reader thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
    
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[6]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 5; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/tabletPcEbook/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }



    this.wLanInternet= function(){
        browser
        //check WLAN & Internet  category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/h2/a','WLAN & Internet')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/h2/a')
        .verify.ok(true,"check WLAN & Internet ")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/wLanInternetCat.png')
        .pause(4000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/a/figure/figcaption')
        .verify.ok(true," check WLAN & Internet  thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
        
    
        //click on the links under this category
        browser
        .pause(4000)
        .useXpath()
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[7]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 3; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/wLanInternet/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }


    this.druckerScanner= function(){
        browser
        //check Drucker & Scanner  category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/h2/a','Drucker & Scanner')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/h2/a')
        .verify.ok(true,"Check Drucker & Scanner")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Drucker&ScannerCat.png')
        .pause(4000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/a/figure/figcaption')
        .verify.ok(true,"Check Drucker & Scanner thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
    
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[8]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 5; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/DruckerScanner/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }

    this.fotoVideo= function(){
        browser
        //check Foto & Video category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/h2/a','Foto & Video')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/h2/a')
        .verify.ok(true,"Check Foto & Video ")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Foto&VideoCat.png')
        .pause(4000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/a/figure/figcaption')
        .verify.ok(true,"Check Foto & Video thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
    
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[9]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 6; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Foto&Video/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }

    this.gaming= function(){
        browser
        //check Gaming & Zubehör category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/h2/a','Gaming & Zubehör')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/h2/a')
        .verify.ok(true,"Check Gaming & Zubehör")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Gaming&ZubehörCat.png')
        .pause(4000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/a/figure/figcaption')
        .verify.ok(true,"Check Gaming & Zubehör thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
    
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[10]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 5; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                //.useXpath()
                //.waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000) //figureout the one link different
                //.verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Gaming/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }


    this.services= function(){
        browser
        //check Services & Dienstleistungen category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[4]/div/div[1]/section/h2/a',299,28)
        .execute(function() { window.scrollTo(0, 1800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/h2/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/h2/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/h2/a','Services & Dienstleistungen')
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/h2/a')
        .verify.ok(true,"Check Services & Dienstleistungen")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Services&DienstleistungenCat.png')
        .pause(4000)
        previews()
        browser
        //click on thumbnail
        .pause(2000)
        .useXpath()
        .click('/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/a/figure/figcaption')
        .verify.ok(true,"Check Services & Dienstleistungen thumbnail")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/div[3]/div/main/div/div[1]")
        previews()
    
        //click on the links under this category
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[4]/div/div[11]/section/ul/li[x]/a"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 3; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/Services/' + linkImage +'.png')
                .pause(4000)
                previews()
              }
            })
    }



    this.aktuelleBestenlisten= function(){
        browser
        //check Aktuelle Bestenlisten
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[5]/div/div/section/div[1]/h3/a',199,25)
        .execute(function() { window.scrollTo(0, 4000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[1]/h3/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[1]/h3/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[5]/div/div/section/div[1]/h3/a','Aktuelle Bestenlisten')
        .pause(2000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AktuelleBestenlisten.png')
        .pause(2000)
        .click('/html/body/div[3]/div/main/div/div[5]/div/div/section/div[1]/h3/a')
        .verify.ok(true,"Check Aktuelle Bestenlisten")
        consentMessage()
        browser
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AktuelleBestenlistenPage.png')
        .pause(4000)
        previews()
        browser
        //click on the big thumbnail
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[5]/div/div/section/article/a/figure/figcaption", 2000)
        .click("/html/body/div[3]/div/main/div/div[5]/div/div/section/article/a/figure/figcaption")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/main/div[2]")
        .verify.ok(true,"click on the big thumbnail of Aktuelle Bestenlisten")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AktuelleBestenlistenPage_1.png')
        .pause(4000)
        previews()

        //click on the first top right side  thumbnail
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[2]/div/article[1]/a/figure/figcaption", 2000)
        .click("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[2]/div/article[1]/a/figure/figcaption")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/main/div[2]")
        .verify.ok(true,"click on first top right side  thumbnail of Aktuelle Bestenlisten")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AktuelleBestenlistenPage_2.png')
        .pause(4000)
        previews()

        //click on the second right side thumbnail
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[2]/div/article[2]/a/figure/figcaption", 2000)
        .click("/html/body/div[3]/div/main/div/div[5]/div/div/section/div[2]/div/article[2]/a/figure/figcaption")
        consentMessage()
        browser
        .useXpath()
        .verify.visible("/html/body/main/div[2]")
        .verify.ok(true,"click on second right side thumbnail of Aktuelle Bestenlisten")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/AktuelleBestenlistenPage_3.png')
        .pause(4000)
        previews()


       
    }



    this.videoEmpfehlungen = function(){
        browser
        //check Video-Empfehlungen 
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[6]',1060,373)
        .execute(function() { window.scrollTo(0, 3800); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[6]/div/section/h3','VIDEO-EMPFEHLUNGEN')
        .verify.ok(true,"Check VIDEO-EMPFEHLUNGEN")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/VIDEO-EMPFEHLUNGEN.png')
        .pause(4000)
        //click on the arrows
        //click on the right arrow
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[2]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[2]")
        //double click to go to the third dot/dot
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[2]")
        .pause(2000)
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[2]")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/VIDEO-EMPFEHLUNGEN_rightArrow.png')
        .pause(4000)

        //click on the right arrow
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[1]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[1]")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[2]/button[1]")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/VIDEO-EMPFEHLUNGEN_leftArrow.png')
        .pause(4000)

        //check dots
        //click on the last dot
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[2]/button[9]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[2]/button[9]")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[2]/button[9]")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/VIDEO-EMPFEHLUNGEN_lastdot.png')
        .pause(4000)
        //click back to the first dot
        .click('/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[2]/button[1]')

        //click on the 3 thumbnails
        //click on first thumbnail
        .pause(3000)
        .waitForElementPresent("//html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/div/div[2]", 2000)
        .verify.visible("//html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/div/div[2]")
        .click("//html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/div/div[2]")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that first thumbnail looks fine")
        .pause(4000)
        previews()
        //click on the link
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/figcaption/div/p", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/figcaption/div/p")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[6]/a/figure/figcaption/div/p")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that first link looks fine")
        previews()



        //click on second thumbnail
        browser
        .useXpath()
        .pause(3000)
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/div/div[2]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/div/div[2]")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/div/div[2]")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that second thumbnail looks fine")
        .pause(4000)
        previews()
        //click on the link
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/figcaption/div/p", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/figcaption/div/p")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[7]/a/figure/figcaption/div/p")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that second link looks fine")
        previews()

        //click on third thumbnail
        browser
        .useXpath()
        .pause(3000)
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/div/div[2]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/div/div[2]")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/div/div[2]")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that third thumbnail looks fine")
        .pause(4000)
        previews()
        //click on the link
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/figcaption/div/p", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/figcaption/div/p")
        .click("/html/body/div[3]/div/main/div/div[6]/div/section/div/div/div/div[1]/div[1]/article[8]/a/figure/figcaption/div/p")
        consentMessage()
        browser
        .useXpath()
        .pause(3000)
        .verify.visible('//*[@id="M1"]')
        .verify.ok(true,"Check that third link looks fine")
        previews()
       
    }

    this.aktuelleTestLinks = function(){
        browser
        //check Aktuelle Tests
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/h3',1060,373)
        .execute(function() { window.scrollTo(0, 4200); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/h3','AKTUELLE TESTS')
        .verify.ok(true,"Check Aktuelle Tests")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleTestLinks.png')
        .pause(4000)

        //click on the links under Aktuelle Tests
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[7]/div/div/div/section[1]/ul/li[x]/article/a/h4"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 8; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleTestLinks/' + linkImage +'.png')
                .pause(4000)
                previews()
                }
            })
    }

    this.hilfreichePraxistipps = function(){
        browser
        //check Aktuelle Tests
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/h3/a',1060,373)
        .execute(function() { window.scrollTo(0, 4200); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/h3/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/h3/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/h3/a','HILFREICHE PRAXISTIPPS')
        .verify.ok(true,"Check HILFREICHE PRAXISTIPPS")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/hilfreichePraxistipps.png')
        .pause(4000)

        //click on the links under HILFREICHE PRAXISTIPPS
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/ul/li[x]/article/a/h4"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 7; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                .useXpath()
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                consentMessage()
                browser
                .pause(4000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/hilfreichePraxistipps/' + linkImage +'.png')
                .pause(4000)
                previews()
                }
            })
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/a','MEHR PRAXISTIPPS')
        .verify.ok(true,"Check MEHR PRAXISTIPPS button")
        .click("/html/body/div[3]/div/main/div/div[7]/div/div/div/section[2]/a")
        consentMessage()
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div", 5000)
        .verify.visible("/html/body/div[3]/div/main/div")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/hilfreichePraxistipps/merhPraxistipps.png')
        .pause(4000)
        previews()
    }

    this.gutscheine = function(){
        browser
        //check Top Gutscheine
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[8]/div/h3/a',1060,373)
        .execute(function() { window.scrollTo(0, 4500); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[8]/div/h3/a", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[8]/div/h3/a")
        .verify.containsText('/html/body/div[3]/div/main/div/div[8]/div/h3/a','Top Gutscheine')
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/TopGutscheine.png')
        .pause(4000)
        .click('/html/body/div[3]/div/main/div/div[8]/div/h3/a')
        .verify.ok(true,"Check Top Gutscheine")
        .useXpath()
        .waitForElementPresent("/html/body/div[3]", 5000)
        .verify.visible("/html/body/div[3]")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/gutscheine.png')
        .pause(4000)
        previews()
        consentMessage()
        
        //click on the links under gutscheine
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[8]/div/div", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[8]/div/div', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[8]/div/div/a[x]"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 9; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                switchingWin2()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body/div[3]/main", 5000)
                .verify.visible("/html/body/div[3]/main")
                switching()
                }
            })

        }

//this is atest

    this.aktuelleBeitrage = function(){
        browser
        //check Aktuelle Beiträge
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[9]/section/h3',1060,373)
        .execute(function() { window.scrollTo(0, 4600); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[9]/section/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[9]/section/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[9]/section/h3','Aktuelle Beiträge')
        .verify.ok(true,"Check Aktuelle Beiträge (News)")
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleBeitrage.png')
        .pause(4000)


        //click on the links under Aktuelle Beiträge
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[9]/section/div/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[9]/section/div/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[9]/section/div/ul/li[x]/article/a/figure/figcaption"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 20; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                consentMessage()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body/div[3]/div/main/div/div", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div")
                previews()
                }
            })

        //Check pagination
        //click on the right arrow
        browser
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[10]/nav/div',949,48)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[10]/nav/div/a[13]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[10]/nav/div/a[13]")
        .click('/html/body/div[3]/div/main/div/div[10]/nav/div/a[13]')
        consentMessage()
        browser
        .useXpath()
        .verify.ok(true,"Check second page by clicking in the left arrow")
        .pause(3000)
        //check if page is ok
        .verify.visible("/html/body/div[3]/div/main/div")
        .pause(3000)
        //.moveToElement('/html/body/div[3]/div/main/div/div[3]/nav/div',949,48)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleBeitrage_rightArrow.png')
        .pause(4000)

        //click on last number 
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[3]/nav/div/a[13]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[3]/nav/div/a[13]")
        .click('/html/body/div[3]/div/main/div/div[3]/nav/div/a[13]')
        consentMessage()
        browser
        .useXpath()
        .verify.ok(true,"Check last number")
        .pause(3000)
        //check if page is ok
        .verify.visible("/html/body/div[3]/div/main/div")
        .pause(3000)
        //.moveToElement('/html/body/div[3]/div/main/div/div[3]/nav/div',949,48)
        .execute(function() { window.scrollTo(0, 1500); }, [])
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleBeitrage_lastPage.png')
        .pause(4000)

        //click on left arrow 
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[3]/nav/div/a[1]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[3]/nav/div/a[1]")
        .click('/html/body/div[3]/div/main/div/div[3]/nav/div/a[1]')
        consentMessage()
        browser
        .useXpath()
        .verify.ok(true,"Check left arrow")
        .pause(3000)
        //check if page is ok
        .verify.visible("/html/body/div[3]/div/main/div")
        .pause(3000)
        //.moveToElement('/html/body/div[3]/div/main/div/div[3]/nav/div',949,48)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .pause(4000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/aktuelleBeitrage_leftArrow.png')
        .pause(4000)
        previews()
        previews()
        previews()
    }





    this.beliebteChipThemen= function(){
        browser
        //check BELIEBTE CHIP-THEMEN category
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/section/h3',199,25)
        .execute(function() { window.scrollTo(0, 2000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/section/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/section/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/section/h3','BELIEBTE CHIP-THEMEN')
        .verify.ok(true,"Check BELIEBTE CHIP-THEMEN")
    
        //click on the links under BELIEBTE CHIP-THEMEN
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/section/div", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/section/div', function(result) {
            var item = "/html/body/div[3]/div/main/div/section/div/a[x]"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 5; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                consentMessage()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body", 5000)
                .verify.visible("/html/body")
                .pause(3000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/beliebteChipThemen/' + linkImage +'.png')
                .pause(3000)
                previews()
              }
            })
    }

  
    this.topArtikel= function(){
        browser
        //check TOP ARTIKEL AUS TEST & KAUFBERATUNG
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/h3',299,17)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/h3','TOP ARTIKEL AUS TEST & KAUFBERATUNG')
        .verify.ok(true,"TOP ARTIKEL AUS TEST & KAUFBERATUNG")
    
        //click on the links under TOP ARTIKEL AUS TEST & KAUFBERATUNG
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/ul/li[x]/article/a/h4"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 8; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                consentMessage()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                .pause(3000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/topArtikel/' + linkImage +'.png')
                .pause(3000)
                previews()
              }
            })
        //click on mehr
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/a/b", 2000)
        .click('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[1]/a/b')
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/topArtikel/mehr.png')
        .pause(3000)
        previews()
    }


    this.topTest= function(){
        browser
        //check TOP TESTS AUS TEST & KAUFBERATUNG
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/h3',299,17)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/h3','TOP TESTS AUS TEST & KAUFBERATUNG')
        .verify.ok(true,"TOP TESTS AUS TEST & KAUFBERATUNG")
    
        //click on the links under TOP ARTIKEL AUS TEST & KAUFBERATUNG
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/ul/li[x]/article/a/h4"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 8; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                consentMessage()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body/div[3]/div/main/div/div[1]", 5000)
                .verify.visible("/html/body/div[3]/div/main/div/div[1]")
                .pause(3000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/topTest/' + linkImage +'.png')
                .pause(3000)
                previews()
              }
            })
        //click on mehr
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/a/b", 2000)
        .click('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[2]/a/b')
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/topTest/mehr.png')
        .pause(3000)
        previews()
    }


    this.neueDownloads= function(){
        browser
        //check NEUE DOWNLOADS BEI CHIP
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/h3',299,17)
        .execute(function() { window.scrollTo(0, 3000); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/h3", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/h3")
        .verify.containsText('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/h3','NEUE DOWNLOADS BEI CHIP')
        .verify.ok(true,"NEUE DOWNLOADS BEI CHIP")
    
        //click on the links under TOP ARTIKEL AUS TEST & KAUFBERATUNG
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/ul", 4000)
        .element('xpath','/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/ul', function(result) {
            var item = "/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/ul/li[x]/article/a/h4"
            var forx = 0
            var linkImage = 0
            for (var i = 0; i < 8; i++) {
                forx = forx+1;
                linkImage = linkImage+1
                let nextLink = item.replace("x", forx);
                console.log("item ->  " + nextLink)
                browser
                .click(nextLink)
                consentMessage()
                browser
                .useXpath()
                .pause(3000)
                .waitForElementPresent("/html/body", 5000)
                .verify.visible("/html/body")
                .pause(3000)
                .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/neueDownloads/' + linkImage +'.png')
                .pause(3000)
                previews()
              }
            })
        //click on mehr
        browser
        .useXpath()
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/a/b", 2000)
        .click('/html/body/div[3]/div/main/div/div[12]/div/div/div/section[3]/a/b')
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/neueDownloads/mehr.png')
        .pause(3000)
        previews()
    }

    this.newsLetter =function(){
        browser
        //check Schnäppchen Newsletter
        .useXpath()
        //.moveToElement('/html/body/div[3]/div/main/div/div[13]/div/section/form/h5',299,17)
        .execute(function() { window.scrollTo(0, 5500); }, [])
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/h5", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/h5")
        .verify.containsText('/html/body/div[3]/div/main/div/div[13]/div/section/form/h5','SCHNÄPPCHEN NEWSLETTER')
        .verify.ok(true,"Check Schnäppchen Newsletter")

        //Check title
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/h2", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/h2")
        .verify.containsText('/html/body/div[3]/div/main/div/div[13]/div/section/form/h2','Schnäppchen-Highlights')
        .verify.ok(true,"Check title in Schnäppchen Newsletter")

        //check description
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/p[1]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/p[1]")
        .verify.containsText('/html/body/div[3]/div/main/div/div[13]/div/section/form/p[1]','Wir helfen Ihnen zu sparen. Erhalten Sie die besten Schnäppchen direkt in Ihr Mailpostfach.')
        .verify.ok(true,"Check description in Schnäppchen Newsletter")

        //Check agreement
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/p[2]", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/p[2]")
        .verify.containsText('/html/body/div[3]/div/main/div/div[13]/div/section/form/p[2]','')
        .verify.ok(true,"Check agreement text in Schnäppchen Newsletter")


        //Check email field, checkbox and send button
        //email field
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/input", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/input")
        .clearValue('/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/input')
        .pause(3000)
        .setValue("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/input", "test@test.test")
        .verify.ok(true,"Enter a value in email field - Schnäppchen Newsletter")
        //Check checkbox text
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[2]/label", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[2]/label")
        .click("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[2]/label")
        .verify.ok(true,"Check checkbox text in Schnäppchen Newsletter")
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/newsLetter1.png')
        .pause(3000)

        //click on send 
        .waitForElementPresent("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/button", 2000)
        .verify.visible("/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/button")
        .verify.containsText('/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/button','ANMELDEN')
        .click('/html/body/div[3]/div/main/div/div[13]/div/section/form/div[1]/div/button')
        .verify.ok(true,"Check email functionality Schnäppchen Newsletter")
        .pause(3000)
        .saveScreenshot('.../POM/POM_TestundKaufberatung/Production/Screenshots_Production/newsLetter_send.png')
        .pause(3000)

    }





return this;
}//end big module

