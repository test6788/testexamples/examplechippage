Hello, this is just a small test for automation test practice. It is in progress to be a bigger coverage.
The main idea or purpose for this test suits is to check in a faster different scenarios. For example: using the search tool, verify all elements to be present, check some functionalities, check urls, and others.
Here some important notes:
To run the test cases from CI/CD JOBS OR PIPELINE

"Production" test cases are selected by default but if you want to run smokeTestProd follow the next:
Test cases are in the folders “Production”, so if we create a new folder like "smokeTestProd", we need to switch the folder name in “conf” folder -> parallel.conf.js -> “src_folders : [ “Folder/folder” ]” field. For example: if you want to check/test "smokeTestProd" then parallel.conf.js should have src_folders : ["smokeTestProd].
Name of the job: .gitlab-ci.yml file and go to the end of the file. Here change the name of the field, for example "test in Production" or "test in Staging" and commit.
Go to CI/CD ->  Jobs -> see the latest and click on the “retry” button under Coverage column. Here it will run it again.
gitlab-ci.yml file  is important because it has the actions for Browserstack (set up environment, install npm…). Here we can add or remove the environments where we want to run the test cases (for example: win10, FF) like env1, env2 (this are in parallel.conf.js ).IMPORTANT: It is better to run it one environment at the time, example: run all test cases first with env1, after that is done run it with env2, because since we have many files sometimes it will not run all of them or give errors.
NOTES:
every change that we do in the project it will run the test cases.
When we run the tests cases on browserStack we don’t save locally a report or screenshots. All stays in “browserstack-build sessions” where we can see video, screenshots and details for the tests.
Credentials for BrowseStack are needed, and we need to add the keys in “conf” folder -> parallel.conf.js
this project does not have available the CI/CD for you to check because you will need to have access to BrowseStack credentials.

To check in detail what was tested from Gitlab:

Go to CI/CD ->  Jobs -> see the latest “Passed”  and click on it.
You can see what was test in there and for more visual understanding go to the section "See more info, video, & screenshots on Browserstack:".

CHECK THE VIDEOS FOR DEMO
Since we cannot run the test in our pipeline because "user" and "password" keys for BrowserStack are needed, you can check the video demo or the screenshots for the tests in "Production"-> "Screenshots"  and download the html report that is in "Production"->"test_output"
To run these test cases in BrowseStack from your local folder and not from Gitlab:

Download the project to your computer.
Open the project in Visual Studio or the code editor of your preference.
Open a terminal in there and access to Staging or Production folder.
Remember to update in nightwatch.conf.js file the test case that you want to run (“src_folders")
Run “npx nightwatch -c ./conf/parallel.conf.js -e env1 --reporter html-reporter.js” (-e default(firefox),safari) The "--reporter html-reporter.js" is to create reports in html in your computer.Remember to edit the screenshot path in the test cases.

To run these test cases locally:

Download the project to your computer.
Open the project in Visual Studio or the code editor of your preference.
Open a terminal in there and access to Staging or Production folder.
Remember to update in nightwatch.conf.js file the test case that you want to run (“src_folders")
Run “npx nightwatch -e chrome --reporter html-reporter.js” (-e default(firefox),safari) The "--reporter html-reporter.js" is to create reports in html in your computer.Remember to edit the screenshot path in the test cases.

Slack notifications:
Notifications only appear when we run the test cases from local, not from gitlab.
![](slack_notification.png)


HTML reports:
The reports at the end of the test only will be created/appear when we run the test cases from local, not from gitlab. you can see an example in /production/tests_output
![](report_example.png)