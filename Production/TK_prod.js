var general = require('../pageObjects/generalProd');


module.exports = {
    "@tags" : ['allTogether'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part I' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },
    '1. Open and check "Test & Kaufberatung" from CHIP home page- general elements': function (browser){
        general(browser).testKaufberatung();
        general(browser).breadcrump();
        general(browser).mainHeadline();
        general(browser).briefDescription();
        general(browser).image();

    },

    '2.Check Alle Bestenlisten, Alle Schnäppchen and Alle Tests von CHIP 365': function(browser){
        general(browser).alleBestenlisten();
        general(browser).alleSchnappchen();
        general(browser).alleCHIP365();
    },

    '3.Check search tool -Valid, invalid, empty searchs': function(browser){
        general(browser).search();
        general(browser).sortieren();
        general(browser).emptySearch();
        general(browser).keywordLabel();
        general(browser).invalidValue();

    },

    '4.Check teasers and categories': function(browser){
        general(browser).testKaufberatung();
        general(browser).aktuelleTestTeaser();
        general(browser).catHandy();
        general(browser).pcnPeripherieCat();
        general(browser).tVnSoundCat();//
        general(browser).notebookCat();
        general(browser).smartwatch();
        general(browser).tabletPcEbook();
        general(browser).wLanInternet();
        general(browser).druckerScanner();
        general(browser).fotoVideo();
        general(browser).gaming();
        general(browser).services();
    },

    '5.Check Aktuelle Bestenlisten': function(browser){
        general(browser).aktuelleBestenlisten();
    },

    '6.Check Video-Empfehlungen': function(browser){
        general(browser).videoEmpfehlungen();  
    },

    '7.Check Aktuelle Test and Hilfreiche Praxistipps': function(browser){
        general(browser).aktuelleTestLinks();  
        general(browser).hilfreichePraxistipps();  
    },
    '8.Check Top Gutscheine': function(browser){
        general(browser).gutscheine();  
    },

    '9.Check Aktuelle Beiträge': function(browser){
        general(browser).aktuelleBeitrage();  
    },

    '10.Check Beliebte Chip Themen': function(browser){
        general(browser).beliebteChipThemen();  
    },

    '11.Check content list footer and newsletter': function(browser){
        general(browser).topArtikel();  
        general(browser).topTest();  
        general(browser).neueDownloads();  
        general(browser).newsLetter();  
    },

    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

