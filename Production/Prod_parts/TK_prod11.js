var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partEleven'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part XI' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '11.Check content list footer and newsletter': function(browser){
        general(browser).testKaufberatung();
        general(browser).topArtikel();  
        general(browser).topTest();  
        general(browser).neueDownloads();  
    },

    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

