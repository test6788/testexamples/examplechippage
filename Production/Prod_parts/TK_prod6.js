var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partSix'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part VI' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '5.Check Aktuelle Bestenlisten': function(browser){
        general(browser).testKaufberatung();
        general(browser).aktuelleBestenlisten();
    },

    '6.Check Video-Empfehlungen': function(browser){
        general(browser).videoEmpfehlungen();  
    },


    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

