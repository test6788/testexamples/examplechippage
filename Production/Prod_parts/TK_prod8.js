var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partEight'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part VIII' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '8.Check Top Gutscheine': function(browser){
        general(browser).testKaufberatung();
        general(browser).gutscheine();  
    },

    
    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

