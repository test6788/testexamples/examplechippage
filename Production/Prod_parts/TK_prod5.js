var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partFive'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part V' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '4.Check teasers and categories - PART IV': function(browser){
        general(browser).testKaufberatung();
        general(browser).fotoVideo();
        general(browser).gaming();
        general(browser).services();
    },
    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

