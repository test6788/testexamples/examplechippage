var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partTen'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part X' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '10.Check Beliebte Chip Themen': function(browser){
        general(browser).testKaufberatung();
        general(browser).beliebteChipThemen();  
    },
    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

