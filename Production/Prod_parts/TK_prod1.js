var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partOne'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part I' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },
    '1. Open and check "Test & Kaufberatung" from CHIP home page- general elements': function (browser){
        general(browser).testKaufberatung();
        general(browser).breadcrump();
        general(browser).mainHeadline();
        general(browser).briefDescription();
        general(browser).image();

    },

    '2.Check Alle Bestenlisten, Alle Schnäppchen and Alle Tests von CHIP 365': function(browser){
        general(browser).alleBestenlisten();
        general(browser).alleSchnappchen();
        general(browser).alleCHIP365();
    },

    '3.Check Newsletter and search tool -Valid, invalid, empty searchs': function(browser){
        general(browser).search();
        general(browser).sortieren();
        general(browser).emptySearch();
        general(browser).keywordLabel();
        general(browser).invalidValue();
        general(browser).testKaufberatung();
        general(browser).newsLetter();  
    },
    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

