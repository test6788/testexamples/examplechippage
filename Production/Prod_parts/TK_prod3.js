var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partthree'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part III' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '4.Check teasers and categories - PART II': function(browser){
        general(browser).testKaufberatung();
        general(browser).tVnSoundCat();
        general(browser).notebookCat();
        general(browser).smartwatch();
    },

  


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

