var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partTwo'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production - Part II' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },
    
    '4.Check teasers and categories -PART I ': function(browser){
        general(browser).testKaufberatung();
        general(browser).aktuelleTestTeaser();
        general(browser).catHandy();
        general(browser).pcnPeripherieCat();
    },

   

    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

