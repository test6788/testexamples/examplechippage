var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partFour'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part IV' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },
   
    '4.Check teasers and categories - PART III': function(browser){
        general(browser).testKaufberatung();
        general(browser).tabletPcEbook();
        general(browser).wLanInternet();
        general(browser).druckerScanner();
    },

    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

