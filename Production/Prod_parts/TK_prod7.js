var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partSeven'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part VII' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '7.Check Aktuelle Test and Hilfreiche Praxistipps': function(browser){
        general(browser).testKaufberatung();
        general(browser).aktuelleTestLinks();  
        general(browser).hilfreichePraxistipps();  
    },
    

    


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

