var general = require('../../pageObjects/generalProd');


module.exports = {
    "@tags" : ['partNine'],
    before: function (browser) {
        general(browser).openBrowser();
        general(browser).consentMessage();
    },
    'TEST SUITE: "Test & Kaufberatung" page -Production -Part IX' : function(browser){
        general(browser).tuk1();
        general(browser).consentMessage();

    },

    '9.Check Aktuelle Beiträge': function(browser){
        general(browser).testKaufberatung();
        general(browser).aktuelleBeitrage();  
    },


    after: function (browser){
        browser.pause(5000);
        browser.end()
    }

}//end module

