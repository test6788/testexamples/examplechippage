nightwatch_config = {
    //88 minutes for full test
    // src_folders : ['Production/Prod_parts/TK_prod1.js','Production/Prod_parts/TK_prod2.js','Production/Prod_parts/TK_prod3.js',
    // 'Production/Prod_parts/TK_prod4.js','Production/Prod_parts/TK_prod5.js','Production/Prod_parts/TK_prod6.js',
    // 'Production/Prod_parts/TK_prod7.js','Production/Prod_parts/TK_prod8.js','Production/Prod_parts/TK_prod9.js',
    // 'Production/Prod_parts/TK_prod10.js','Production/Prod_parts/TK_prod11.js'],

    //37 minutes for smoke test to run
    src_folders : ['smokeTestProd/TK_SmokeTestProd.js'],


    selenium : {
      "start_process" : false,
      "host" : "hub-cloud.browserstack.com",
      "port" : 80
    },
    common_capabilities: {
      'build': 'browserstack-build-1',
      'browserstack.user': '',
      'browserstack.key': '',
      'browserstack.debug': true
    },
    test_settings: {
      default: {},
      env1: {
        desiredCapabilities: {
          "browser": "chrome",
          "browser_version": "latest",
          "os": "Windows",
          "os_version": "10"
        }
      },
      env2: {
        desiredCapabilities: {
          "browser": "firefox",
          "browser_version": "latest",
          "os": "Windows",
          "os_version": "10"
        }
      },
      env3: {
        desiredCapabilities: {
          "browser": "safari",
          "browser_version": "14.0",
          "os": "OS X",
          "os_version": "Big Sur"
        }
      }
    }
  };
  // Code to support common capabilities
  for(var i in nightwatch_config.test_settings){
    var config = nightwatch_config.test_settings[i];
    config['selenium_host'] = nightwatch_config.selenium.host;
    config['selenium_port'] = nightwatch_config.selenium.port;
    config['desiredCapabilities'] = config['desiredCapabilities'] || {};
    for(var j in nightwatch_config.common_capabilities){
      config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
    }
  }
  module.exports = nightwatch_config;