//var personal = require('./config');
var newGradesMsg = {
  text: 'Something went wrong!',
  username: 'incoming-webhook',
  icon_emoji: ':ghost:'
};
var noNewGradesMsg = {
  text: 'All test successful',
  username: 'incoming-webhook',
  icon_emoji: ':ghost:'
}; 
var errorMsg = {
  text: 'An error occured while checking!',
  username: 'incoming-webhook',
  icon_emoji: ':ghost:'
}
var slackReporterOptions = {
  slack_message: function(results, options) 
  {
    if (results.failed > 0) { // at least one test failed
      if (results.passed < 2) {
        console.log("An error occured while checking!");
        return errorMsg;
      }
      // only the final test failed -> a new grade must be there
      console.log("Something went wrong!");
      return newGradesMsg; 
    } else { // all tests successful
      console.log("All test successful");
      return noNewGradesMsg;
    }
  },
  slack_webhook_url: 'https://hooks.slack.com/services/heretheidofyourslackchannel',
  slack_send_only_on_failure: true,
  slack_send_only_failed_tests: true
}

const chromedriver = require('chromedriver');
module.exports = {
  reporter: (require('nightwatch-slack-reporter')(slackReporterOptions)),
  before: function (done) {
    chromedriver.start();
    done();
},

after: function (done) {
    chromedriver.stop();
    done();
}
  
};